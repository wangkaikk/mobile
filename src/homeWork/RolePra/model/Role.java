package homeWork.RolePra.model;

public class Role {
    private String name;
    private String desc;
    public void setName(String name) { this.name = name; }
    public String getName(){ return name; }
    public void setDesc(String desc) { this.desc = desc; }
    public String getDesc(){ return desc; }
}
