package homeWork.RolePra.Dao;



import homeWork.RolePra.model.Role;

import java.util.ArrayList;

public class RoleServiceImpl implements RoleService{
    static ArrayList<Role> ROLES = new ArrayList<>();

    @Override
    public void addRole(Role role) {
        ROLES.add(role);
    }
    //返回一个集合，需要建立一个新的集合去接受
    @Override
    public ArrayList<Role> getRoles() {
        return ROLES;
    }
}
