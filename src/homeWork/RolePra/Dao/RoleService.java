package homeWork.RolePra.Dao;


import homeWork.RolePra.model.Role;

import java.util.ArrayList;

public interface RoleService {
    void addRole(Role role);
    ArrayList<Role> getRoles();
}
