package homeWork.RolePra.service;

import homeWork.RolePra.Dao.RoleService;
import homeWork.RolePra.Dao.RoleServiceImpl;
import homeWork.RolePra.model.Role;

import java.util.ArrayList;

public class RoleServiceText {
    public static void main(String[] args) {
        RoleService roleService = new RoleServiceImpl();
        Role role = new Role();
        role.setName("admin");
        role.setDesc("系统管理员");
        roleService.addRole(role);
        Role role1 = new Role();
        role1.setDesc("设计师");
        role1.setName("design");
        roleService.addRole(role1);
        Role role2 = new Role();
        role2.setName("dev");
        role2.setDesc("开发者");
        roleService.addRole(role2);
        ArrayList<Role> roles = new ArrayList<>();
        roles = roleService.getRoles();
        Role lastRole = roles.get(roles.size() - 1);
        System.out.println(lastRole.getName() + ":" + lastRole.getDesc());
    }
}
