package homeWork.Practise;

import homeWork.Object_Pra.Text2;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class TestDay4 {
   /* String date;
    class Time{
        String clock;
    }
    public static void main(String[] args){
        TestDay4 t1 = new TestDay4();
        TestDay4.Time t = t1.new Time();//外部类类名.内部类类名 xx = 外部类对象名.new 内部类类名()
    }*/

   public static void main(String[] args){
       /*new A(){
           @Override
           public void test() {
               System.out.println("Kk.");
           }
       };*/

       /*TestDay4 t = new TestDay4(){
           @Override
           public String toString() {
               return "Kk. is cool.";
           }
       };
       TestDay4 t2 = new TestDay4();
       System.out.println(t.getClass());
       System.out.println(t.hashCode());
       System.out.println(t.toString());
       System.out.println(t2.toString());
       System.out.println(t.equals(t2));*/

       /*Integer x = 100;
       Integer i = new Integer(20);
       Double h = new Double(30);
       double a = new Double(20.2);
       System.out.println(x);
       System.out.println(i);
       System.out.println(h);
       System.out.println(a);*/

       //DecimalFormat类的对象，并指定格式
       /*DecimalFormat df1 = new DecimalFormat("0.0");
       DecimalFormat df2 = new DecimalFormat("#.#");
       DecimalFormat df3 = new DecimalFormat("000.000");
       DecimalFormat df4 = new DecimalFormat("###.###");
       Scanner scan = new Scanner(System.in);
       System.out.print("请输入一个float类型的数字：");
       float f = scan.nextFloat();
       // 对输入的数字应用格式，并输出结果
       System.out.println("0.0 格式：" + df1.format(f));
       System.out.println("#.# 格式：" + df2.format(f));
       System.out.println("000.000 格式：" + df3.format(f));
       System.out.println("###.### 格式：" + df4.format(f)); */

       /*Runtime rt = Runtime.getRuntime();
       System.out.println("jvm的最大内存量"+rt.maxMemory());
       System.out.println("jvm的空闲内存量"+rt.freeMemory());
       String str = "hello"+"world";
       for (int i = 0; i < 100000; i++) {
           str = str + i;
       }//设置方法频繁消耗垃圾内存
       System.out.println("执行string后的内存空闲量"+rt.freeMemory());
       rt.gc();//释放垃圾内存
       System.out.println("释放垃圾后的内存空闲量"+rt.freeMemory());*/

      /* List l = new ArrayList();
       List l2 = new ArrayList(66);
       List l3 = new ArrayList(l2);
      System.out.println(l.isEmpty());
       l.add(2);
       l.add(9);
       l.add(8);
       l.add(10);
       l.add(11);
       l.set(0,1);
      System.out.println(l.iterator());
      System.out.println(l);
      System.out.println(l.get(0));
      System.out.println(l.contains(7));
      l.remove(2);
      System.out.println(l);
      l.clear();
      System.out.println(l);
      System.out.println(l.lastIndexOf(9));
      System.out.println(l.indexOf(9));
      System.out.println(l.subList(1,3));
      System.out.println(l.size());

      Iterator iter = l.iterator();
      while(iter.hasNext()){
         int j = (int)iter.next();
         System.out.println(j);
      }*/
   }
}
/*interface A{
    void test();
}*/