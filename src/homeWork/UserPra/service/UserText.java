package homeWork.UserPra.service;

import homeWork.UserPra.dao.UserDaoImpl;
import homeWork.UserPra.model.User;

import java.util.Scanner;

public class UserText {
    public static void main(String[] args) {
        while(true){
            System.out.println("欢迎进入注册登录界面：");
            System.out.println("1.注册");
            System.out.println("2.登录");
            System.out.println("3.退出");
            UserDaoImpl u = new UserDaoImpl();
            Scanner scanner = new Scanner(System.in);
            String choice = scanner.nextLine();
            switch (choice){
                case "1" :
                    System.out.println("欢迎来到注册界面！");
                    System.out.println("请输入账号");
                    String account = scanner.nextLine();
                    System.out.println("请输入密码：");
                    String password = scanner.nextLine();
                    User user = new User(account,password);
                    u.regist(user);
                    System.out.println("注册成功!");
                    break;
                case "2" :
                    System.out.println("欢迎来到登录界面！");
                    System.out.println("请输入账号：");
                    String inputUserName = scanner.nextLine();
                    System.out.println("请输入密码：");
                    String inputPassword = scanner.nextLine();
                    boolean flag = u.isLogin(inputUserName,inputPassword);
                    if(flag){
                        System.out.println("登录成功！");
                    }
                    else{
                        System.out.println("登录失败！");
                    }
                    break;
                case "3" :
                    System.out.println("成功退出系统！");
                default :
                    System.exit(0);
                    break;
            }
        }
    }
}
