package homeWork.UserPra.dao;

import homeWork.UserPra.model.User;

public interface UserDao {
    public abstract void regist(User user);
    public abstract boolean isLogin(String account , String password);
}
