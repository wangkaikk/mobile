package homeWork.UserPra.dao;


import homeWork.UserPra.model.User;

import java.util.ArrayList;

public class UserDaoImpl implements UserDao {
    static ArrayList<User> users = new ArrayList<User>();

    @Override
    public void regist(User user) {
        users.add(user);
    }

    @Override
    public boolean isLogin(String account, String password) {
        boolean flag = false;
        for (User user : users) {
            if (account.equals(user.getAccount()) && password.equals(user.getPassWord())) {
                flag = true;
                break;
            }
        }
        return flag;
    }
}
