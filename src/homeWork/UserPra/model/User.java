package homeWork.UserPra.model;

public class User {
    private String userName;
    private String account;
    private String passWord;
    private String email;
    public void setUserName(String userName){ this.userName = userName; }
    public String getUserName(){ return userName; }
    public void setAccount(String account){ this.account = account; }
    public String getAccount(){ return account; }
    public void setPassWord(String passWord){ this.passWord = passWord; }
    public String getPassWord(){ return passWord; }
    public void setEmail(String email){ this.email = email; }
    public String getEmail(){ return email; }
    public User(String account , String passWord){
        this.account = account;
        this.passWord = passWord;
    }
}
