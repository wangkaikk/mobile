package homeWork.Object_Pra;

public abstract class Door {
    abstract void putOn();
    abstract void Close();
}
interface function{
    abstract void fireproof();
    abstract void waterproof();
    abstract void guard();
}
class NewDoor extends Door implements function{
    @Override
    void putOn() {
        System.out.println("开门");
    }

    @Override
    void Close() {
        System.out.println("关门");
    }

    @Override
    public void fireproof() {
        System.out.println("这门防火，得劲儿");
    }

    @Override
    public void waterproof() {
        System.out.println("这门防水，厉害");
    }

    @Override
    public void guard() {
        System.out.println("这门还防盗，太六了");
    }

    public static void main(String[] args) {
        NewDoor n = new NewDoor();
        n.putOn();
        n.Close();
        n.fireproof();
        n.waterproof();
        n.guard();
    }
}
