package homeWork.Object_Pra;

import java.util.Scanner;

public class Cook {
    private String food;
    public String getName(){
        return food;
    }
    public void setName(String name){
        this.food = name;
    }
}
class CoolTest{
    public static void main(String[] args) {
        Cook c = new Cook();
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();
        c.setName(name);
        String d = c.getName();
        System.out.println(d);
    }
}