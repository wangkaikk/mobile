package homeWork.Object_Pra;

public class Player {
    private static int sum;
    private Player(){}
    public static Player create(){
        sum = 1;
        Player player = null;
        while(sum <= 11){
            player = new Player();
            System.out.println("创建了"+sum+"个对象");
            sum++;
        }
        System.out.println("对不起，已经创建了11个对象，不能再创建对象了");
        return player;
    }

    public static void main(String[] args) {
        Player.create();
    }
}
