package homeWork.Object_Pra;

public class Vehicle {
    static String brand;
    String color;
    double speed;
    Vehicle(){
        speed = 0;
    }
    Vehicle(String brand,String color,double speed){
        this.brand = brand;
        this.color = color;
        this.speed = speed;
    }
    void run(){
        System.out.println("汽车正以"+speed+"速度行驶");
    }

    public static void main(String[] args) {
        Vehicle v = new Vehicle();
        v.brand = "benz";
        v.color = "black";
        System.out.println("汽车的品牌为"+v.brand+"汽车颜色为"+v.color);
    }
}
