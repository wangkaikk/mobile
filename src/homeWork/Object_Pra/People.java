package homeWork.Object_Pra;

import java.util.Scanner;

public class People {
    String name;
    String work;
    int year;
    public void Message(String name, String work, int year){
        this.name = name;
        this.work = work;
        this.year = year;
        System.out.println("姓名为"+this.name+"，工作为"+this.work+",年龄为"+this.year);
    }
}
class Student2 extends People{
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String a = scanner.nextLine();
        String b = scanner.nextLine();
        int c = scanner.nextInt();
        People stu1 = new Student2();
        stu1.Message(a,b,c);
    }
}
class Worker extends People{
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String d = scanner.nextLine();
        String e = scanner.nextLine();
        int f = scanner.nextInt();
        People w1 = new Worker();
        w1.Message(d,e,f);
    }
}