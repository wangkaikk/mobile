package homeWork.Object_Pra;

public interface USB{
   abstract void function2();
}
class Printer implements USB{
    @Override
    public void function2() {
        System.out.println("打印机连接USB后打印");
    }

    public static void main(String[] args) {
        Printer p = new Printer();
        p.function2();
    }
}
class Keyboard implements USB{
    @Override
    public void function2() {
        System.out.println("键盘连接USB后输入");
    }

    public static void main(String[] args) {
        Keyboard k = new Keyboard();
        k.function2();
    }
}