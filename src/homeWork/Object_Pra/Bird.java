package homeWork.Object_Pra;

public class Bird {
    public void fly(){
        System.out.println("一只鸟正在飞翔");
    }
}
class Ostrich extends Bird {
    @Override
    public void fly() {
        super.fly();
        System.out.println("鸵鸟不会飞");
    }

    public static void main(String[] args) {
        Bird o = new Ostrich();
        o.fly();
       boolean o2 =o instanceof Bird;
        System.out.println(o2);
        boolean o3 = o instanceof Ostrich;
        System.out.println(o3);
    }
}
class Pigeon extends Bird{
    public void fly() {
        super.fly();
        System.out.println("鸽子会飞");
    }

    public static void main(String[] args) {
        Bird p = new Pigeon();
        p.fly();
    }
}