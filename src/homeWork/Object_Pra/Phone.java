package homeWork.Object_Pra;

public class Phone {
    String lang;
    String color;
    int memory;
    String cpu;
    public Phone(){
        lang = "English";
    }
    public Phone(String language){
        lang = language;
    }
    public static void main(String[] args) {
     Phone ph1 = new Phone();
        System.out.println(ph1.lang);
        Phone ph2 = new Phone("中文简体");
        System.out.println(ph2.lang);
    }
}

