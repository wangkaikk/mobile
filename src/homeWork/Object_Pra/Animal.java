package homeWork.Object_Pra;


public class Animal {
    public void Eat(){
        System.out.println("动物正在吃食物");
    }

    public static void main(String[] args) {
        Animal e = new Eagle();
        e.Eat();
        Animal f = new Frog();
        f.Eat();
        Animal c = new Caterpillar();
        c.Eat();
    }
}
class Eagle extends Animal{
    public void Eat(){
        System.out.println("老鹰正在吃肉");
    }
}
class Frog extends Animal{
    public void Eat(){
        System.out.println("青蛙正在吃虫子");
    }
}
class Caterpillar extends Animal{
    public void Eat(){
        System.out.println("毛毛虫正在吃树叶");
    }
}
