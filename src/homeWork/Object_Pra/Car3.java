package homeWork.Object_Pra;

public class Car3 {
    int oilLevel=10;
    public void toOilStation(Car3 m)
    {
        OilStation a1=new OilStation();
        a1.add(m);
    }
}
class GasCar extends Car3{
    GasCar m1=new GasCar();

}
class DieCar extends Car3{
    DieCar m2=new DieCar();
}
class OilStation{
    public void add(Car3 car){
        if(car instanceof GasCar)
        {
            GasCar c1=(GasCar)car;
            c1.oilLevel+=10;
        }
        if(car instanceof DieCar)
        {
            DieCar c2=(DieCar)car;
            c2.oilLevel+=20;
        }
    }
}