package homeWork.Object_Pra;

import java.util.Scanner;

public class Dog {
    private String name;
    private String color;
    private String variety;
    private double weight;
    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }
}
class DogTest{
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Dog d = new Dog();
        String name = scanner.nextLine();
        d.setName(name);
        System.out.println(d.getName());
    }
}
