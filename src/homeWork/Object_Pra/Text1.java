package homeWork.Object_Pra;

import java.util.Scanner;

public class Text1 {
    public static void main(String[] args) {
        BankAccount Tom = new BankAccount();
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        Tom.NowMoney(a);
        int b = scanner.nextInt();
        Tom.GetBalance(b);
        int c = scanner.nextInt();
        Tom.WithDraw(c);
    }
}
class BankAccount{
    int money;
    BankAccount(){
        money = 0;
    }
    void NowMoney(int money){
        this.money = money;
        System.out.println(money);
    }
    void GetBalance(int TakeMoney){
        money = money - TakeMoney;
        System.out.println(money);
    }
    void WithDraw(int GetMoney){
        money = money + GetMoney;
        System.out.println(money);
    }
}
