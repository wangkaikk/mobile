package homeWork.Loop_Pra;

public class Loop3 {
    public static void main(String[] args) {
        int sum1 = 0;
        int sum2 = 0;
        for (int i = 1; i <= 100; i++) {
            if(i % 2 == 0){
                sum2 += i;
            }
            else{
                sum1 += i;
            }
        }
        System.out.printf("奇数和为%d,偶数和为%d",sum1,sum2);
    }
}
