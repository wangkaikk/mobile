package homeWork.Loop_Pra;

import java.util.Random;
import java.util.Scanner;

public class Loop2 {
    public static void main(String[] args) {
        Random random = new Random();
        int a = random.nextInt(50)+50;
        int b;
        Scanner scanner = new Scanner(System.in);
        do{
            b = scanner.nextInt();
            if(b == a) {
                System.out.println("输入正确");
            }
            else if(b < a){
                System.out.println("输入的数字过小");
            }
            else{
                System.out.println("输入的数过大");
            }
        }while(b != a);
        scanner.close();
    }
}
