package homeWork.Loop_Pra;

public class Loop4 {
    public static void main(String[] args) {
        int[] a = new int[11];
        a[0] = 1;
        a[1] = 1;
        for(int i = 2; i < 11; i++){
            a[i] = a[i-1] + a[i-2];
        }
        System.out.printf("第十一个数字为%d",a[10]);
    }
}
