package homeWork.Array_Pra;

import java.util.Scanner;

public class array2 {
    public static void main(String[] args) {
        int[] scores = new int[3];
        int sum = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入三门课程的成绩：");
        for (int i = 0; i < 3; i++) {
            scores[i] = scanner.nextInt();
            sum += scores[i];
        }
        double aver = sum/3;
        System.out.println("三门课程的平均成绩为"+String.format("%.2f",aver));
    }
}
