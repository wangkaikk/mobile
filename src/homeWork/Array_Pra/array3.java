package homeWork.Array_Pra;

public class array3 {
    public static void main(String[] args) {
        int[] arr= {65,89,56,78,45,69,59};
        int temp;
        for (int i = 0; i < arr.length-1; i++) {
            for(int j = 0;j < arr.length-1-i;j++){
                if(arr[j] >= arr[j+1]){
                    temp = arr[j+1];
                    arr[j+1] = arr[j];
                    arr[j] = temp;
                }
            }
        }
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }
    }
}
