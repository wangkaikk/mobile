package homeWork.Array_Pra;


public class array6 {
    public static void main(String[] args) {
        String[][] a = new String[4][3];
        for (int i = 0; i < 4; i++) {
            for(int j = 0; j < 3; j++){
                if(i == 0 && j == 0)
                {
                    a[i][j] = "历史书籍";
                }
                else if(i == 0 && j == 2){
                    a[i][j] = "地理书籍";
                }
                else if(i == 1 && j == 1){
                    a[i][j] = "科学书籍";
                }
                else if(i == 2 && j == 2){
                    a[i][j] = "军事书籍";
                }
                else if(i == 3 && j == 0){
                    a[i][j] = "新闻书籍";
                }
                else
                    a[i][j] = "空白书籍";
            }
        }
        for (int i = 0; i < 4; i++) {
            for(int j = 0; j < 3; j++){
                System.out.print("\t"+a[i][j]);
            }
            System.out.print("\n");
        }
    }
}
