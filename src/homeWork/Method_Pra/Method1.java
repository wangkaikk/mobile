package homeWork.Method_Pra;

import java.util.Scanner;

public class Method1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double[] a = new double[10];
        for(int i = 0; i < 10; i++){
            a[i] = scanner.nextDouble();
        }
        double max = MethodMax(a);
        double min = MethodMin(a);
        System.out.println("max="+max);
        System.out.println("min="+min);
        scanner.close();
    }
    public static double MethodMax(double[] a){
        double m = 0;
        for (int i = 0; i < 9; i++) {
            if(a[i] <= a[i+1])
                m = a[i+1];
        }
        return m;
    }
    public static double MethodMin(double[] a ){
        double n = 0;
        for (int i = 0; i < 9; i++) {
            if(a[i] <= a[i+1])
                n = a[i];
        }
        return n;
    }
}
