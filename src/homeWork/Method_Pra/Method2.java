package homeWork.Method_Pra;


public class Method2 {
    public static void main(String[] args) {
        int[] a = new int[]{2,6,3,5,4,1,9,30,20};
        int[] b = MethodSort(a);
        for (int i = 0; i < a.length; i++) {
            System.out.println(b[i]);
        }
    }
    public static int[] MethodSort(int[] a){
        int temp;
        for (int i = 0; i < a.length-1; i++) {
            for(int j = 0; j < a.length-1-i; j++){
                if(a[j] >= a[j+1]){
                    temp = a[j+1];
                    a[j+1] = a[j];
                    a[j] = temp;
                }
            }
        }
        return a;
    }
}
