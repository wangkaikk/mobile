package homeWork.Method_Pra;

import java.util.Scanner;

public class Method5 {
    public static void main(String[] args) {
        int a,b;
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入两个正数：");
        a = scanner.nextInt();
        b = scanner.nextInt();
        Exchange(a,b);
    }
    public static void Exchange(int a,int b) {
        int c;
        c = b;
        b = a;
        a = c;
        System.out.println("交换后顺序为："+a+b);
    }
}
