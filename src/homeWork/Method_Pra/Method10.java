package homeWork.Method_Pra;

import java.util.Scanner;

public class Method10 {
    public static void main(String[] args) {
        int n,x,y;
        x = 0;
        y = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入学生位数：");
        n = scanner.nextInt();
        double[] a = new double[n];
        System.out.println("请输入各位学生的成绩（要求成绩在0-100之间）：");
        for (int i = 0; i < n; i++) {
            a[i] = scanner.nextDouble();
            if(a[i] >= 60) {
                x++;
            }
            else{
                y++;
            }
        }
        System.out.println("及格人数为："+x);
        System.out.println("不及格人数为："+y);
}
}
