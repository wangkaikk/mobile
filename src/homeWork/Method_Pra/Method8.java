package homeWork.Method_Pra;

import java.util.Scanner;

public class Method8 {
    public static void main(String[] args) {
        int a,b;
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入两个正数：");
        a = scanner.nextInt();
        b = scanner.nextInt();
        Exchange2(a,b);
    }
    public static void Exchange2(int a,int b){
        int c;
        c = b;
        b = a;
        a = c;
        System.out.println("交换位置后为"+a+b);
    }
}
