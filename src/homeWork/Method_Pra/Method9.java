package homeWork.Method_Pra;

import java.util.Scanner;

public class Method9 {
    public static void main(String[] args) {
        int n;
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入评委数：");
        n = scanner.nextInt();
        double[] a = new double[n];
        System.out.println("请输入各位评委打出的成绩：");
        for (int i = 0; i < n; i++) {
            a[i] = scanner.nextDouble();
        }
        Average2(a,n);
    }
    public static void Average2(double[] a,int n){
        double temp;
        for (int i = 0; i < n-1; i++) {
            for(int j = 0; j < n-1-i; j++){
                if(a[j] >= a[j+1]){
                    temp = a[j+1];
                    a[j+1] = a[j];
                    a[j] = temp;
                }
            }
        }
        double sum = 0;
        for (int i = 1; i < n-1; i++) {
            sum += a[i];
        }
        double aver = sum/n;
        System.out.println("平均分为"+String.format("%.2f", aver));
    }
}
