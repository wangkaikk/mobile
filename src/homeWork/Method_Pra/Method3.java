package homeWork.Method_Pra;

import java.util.Scanner;

public class Method3 {
    public static void main(String[] args) {
        System.out.println("请输入两个整数");
        Scanner scanner = new Scanner(System.in);
        int a,b,e,sum1;
        a = scanner.nextInt();
        b = scanner.nextInt();
        sum1 = MethodSum1(a,b);
        System.out.println("这两个整数的和："+sum1);
        System.out.println("请输入两个小数");
        double c,d,f,sum2,sum3;
        c = scanner.nextDouble();
        d = scanner.nextDouble();
        sum2 = MethodSum2(c,d);
        System.out.println("这两个小数的和："+sum2);
        System.out.println("请输入一个整数和一个小数");
        e = scanner.nextInt();
        f = scanner.nextDouble();
        sum3 = MethodSum3(e,f);
        System.out.println("这两个数的和："+sum3);
        scanner.close();
    }
    public static int MethodSum1(int a,int b){
        int s1 = a + b;
        return s1;
    }
    public static double MethodSum2(double c,double d){
        double s2 = c + d;
        return s2;
    }
    public static double MethodSum3(int e,double f){
        double s3 = e + f;
        return s3;
    }
}
